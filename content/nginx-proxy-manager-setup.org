#+TITLE: Setting up Nginx Proxy Manager, Autorun Using SystemD, Run as User
#+DATE: 2023-08-30
#+TAGS: self-hosted docker nginx

I need a reverse proxy and to be honest, I'd like it to be as simple as possible. NPM [0] looks really simple. I'd also like to not have to worry about restarting containers when my server goes down.

* =docker-compose.yml= [0]

We begin with the following =docker-compose.yml=:

#+begin_src docker-compose
version: '3.8'
services:
  npm:
    image: 'jc21/nginx-proxy-manager:latest'
    restart: unless-stopped
    ports:
      - '80:80'
      - '81:81'
      - '443:443'
    volumes:
      - ./data:/data
      - ./letsencrypt:/etc/letsencrypt
#+end_src

* Running services as a user

Let's not use root to run the services. Append the following to the above file right after the image name.

#+begin_src docker-compose
    environment:
      PUID: 1000
      PGID: 1000
#+end_src

* Using a Network for All Upstream Services

Because all of our services will be using docker containers, we can create a docker network to hook all of the containers into.

=docker network create friedsam=

We need to add the network to our docker compose. NOTE: this block should not be indented at all; it is not a subsection of the =services= block.

#+begin_src docker-compose
networks:
  defautl:
    external: true
    name: friedsam
#+end_src

We need to add this section to every =docker-compose.yml= we want running on this network.

* Run as a SystemD Service [1]

I'd like this (and other) docker images to re-start if my server loses power. To do this, we'll use userspace systemd services.

We'll be keeping our service files in =~/.config/systemd/user=. This first file is a template that will call all files in a given directory. We've made some small changes from [1] so that the compose files can live in our user's home drive.

#+begin_src service_file
[Unit]
Description=%i service with docker compose
PartOf=docker.service
After=docker.service

[Service]
Type=oneshot
RemainAfterExit=true
WorkingDirectory=/home/web/docker/services/%i
ExecStart=/usr/local/bin/docker-compose up -d --remove-orphans
ExecStop=/usr/local/bin/docker-compose down

[Install]
WantedBy=default.target
#+end_src

We change the =WantedBy= field to be =default.target= so that this service kicks off when the machine starts [2].

* Links

- [0] https://nginxproxymanager.com/
- [1] https://gist.github.com/mosquito/b23e1c1e5723a7fd9e6568e5cf91180f
- [2] https://superuser.com/a/1028180
