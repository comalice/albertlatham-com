#+TITLE: Arch Linux, Nebula, Defined Network (dnclient), and no tun/tap devices.
#+DATE: 20230411
#+TAGS: arch nebula defined tun-tap

# The Problem - `dnclient` doesn't start, and no `defined1` network interface is created.

I have Proxmox running on a server, and because I'm an Arch Linux native I decided I wanted to use arch linux as my base service container.

Proxmox already has an Arch Linux LXC template available [0], so I created my base container through the UI using that [1]. I'm using the Nebula mesh network as a VPN layer to link k8s nodes together. If I didn't do this, I'd have to manually specify IP addresses on all of my containers when I create them. It isn't really a "problem", but I decided I'd rather not do that. Thus my solution of "allow a VPN mesh network to handle the static IP addresses for the k8s cluster".

The problem arises when, after enrolling a Nebula node, if you check `ip addr` on the node there is no tun/tap device named `defined1`. And there should be.


# The Solution - add some lines to the lxc configuration in Proxmox

The solution is well known, and actually pretty simple [2].

Open your Proxmox console and navigate to `/etc/pve/lxc`.

For your arch linux container config (you'll need to know the container number), open it and add the following two lines:

```
lxc.cgroup.devices.allow: c 10:200 rwm
lxc.mount.entry: /dev/net dev/net none bind,create=dir
```

NOTE: Some folks in [2] discovered that `cgroup` actually needs to be `cgroup2`, so if the above doesn't work, try changing it.

# Aside - initial setup of the container.

If you're setting up an arch linux container for the first time, see [3] for a quickstart guide.

# Sources

- [0] https://pve.proxmox.com/wiki/Linux_Container#pct_container_images
- [1] https://linuxhandbook.com/proxmox-create-container/
- [2] https://forum.proxmox.com/threads/how-to-enable-tun-tap-in-a-lxc-container.25339/
- [3] https://nathanlabadie.com/proxmox-quickly-building-arch-containers/
