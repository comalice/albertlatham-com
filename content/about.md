+++
date = "2023-03-23"
title = "About"
+++

## Christ, Breaking, Making

In a few words, I am first a follower of the God of the Bible; Christ is my saviour.

Second, I like to break things. I started early in life breaking things. Later in life I learned to fix things and eventually make things.

I don't have a software engineering specialty to speak of, and my interests are broad. "Jack of all trades, master of none". I have experience with Python, JS, Java, C, C#, Scheme, and a few other programming languages. One of them is quite fun, go take a look at the [TerraTerm spec](https://ttssh2.osdn.jp/index.html.en) if you're looking for an interesting read.
